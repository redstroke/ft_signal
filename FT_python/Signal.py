import time
import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np
import pylab
import random
import math

def ft_pram(times, amplitudes, omegas):
    a = np.asarray(amplitudes, dtype=float)
    o = np.asarray(omegas, dtype=float)
    t = np.asarray(times, dtype=float)
    d_t = t[1] - t[0]
    X = np.zeros((len(o),), dtype=np.complex128)
    for i in range(len(o)):
        X[i] = np.dot(a, np.exp(-1j * o[i] * t))
        if i == 0:
            print(X[i])
    return X*d_t

def ft_obr(times, spectrum, omegas):
    s = np.asarray(spectrum, dtype=complex)
    o = np.asarray(omegas, dtype=float)
    t = np.asarray(times, dtype=float)
    d_o = o[1] - o[0]
    x = np.zeros((len(t),))
    for i in range(len(t)):
        x[i] = np.dot(s, np.exp(1j * o * t[i])).real
    return x * (d_o / np.pi)

def dft_pram(amplitudes):
    x = np.asarray(amplitudes, dtype=float)
    N = x.shape[0]
    n = np.arange(N)
    k = n.reshape((N,1))
    M = np.exp(-2j*np.pi*k*n/N)

    return np.dot(M,x)

def dft_obr(spectrum):
    x = np.asarray(spectrum, dtype=complex)
    N = x.shape[0]
    n = np.arange(N)
    k = n.reshape((N,1))
    M = np.exp(2j*np.pi*k*n/N)

    return (np.dot(M,x).real)/N


def task(times, amplitudes):
    #обычное преобразование фурье
    f = np.arange(0, 200, 0.1)
    omegas = f*2*np.pi
    
    #дискретное и быстрое преобразование фурье
    #N = len(amplitudes)
    #delta_t = times[1] - times[0]
    #delta_omega = 2 * np.pi / (N * delta_t)
    #omegas = np.array([k * delta_omega for k in range(N)])

    
    spectrum = ft_pram(times, amplitudes, omegas) #обычное преобразование фурье
    #spectrum = dft_pram(amplitudes) #дискретное преобразование фурье
    #spectrum = np.fft.fft(amplitudes) #быстрое преобразование фурье
    
    ax1 = fig.add_subplot(2, 1, 2)
    ax1.plot(omegas / (2 * np.pi), abs(spectrum), '', label="Спектр")
    ax1.set_xlabel("Частота")
    ax1.set_ylabel('Амплитуда')
    ax1.legend()

    restored_signal = ft_obr(times, spectrum, omegas) #обычное преобразование фурье
    #restored_signal = dft_obr(spectrum) #дискретное преобразование фурье
    #restored_signal = np.fft.ifft(spectrum) #быстрое преобразование фурье

    fig2 = pylab.figure(2)
    ax2 = fig2.add_subplot(2, 1, 1)
    ax2.plot(times, restored_signal, 'r', label="Восстановленный", linestyle='--')
    ax2.set_xlabel("Время, с")
    ax2.set_ylabel('Амплитуда')

    ax.plot(times, restored_signal, 'r', label="Восстановленный сигнал", linestyle='--')

    ax.legend()

start = time.time()
x = []
y = []
x1 = []
y1 = []
x, y = wavfile.read('Sig/songg.wav', 'r')
x2 = np.arange(0, (len(y)*2/100000) - 0.000002, 0.00002)

for i in range(len(y)):
    if i % 100 == 0:
        y1.append(y[i])
        x1.append(x2[i])

fig = pylab.figure(1)
ax = fig.add_subplot(2, 1, 1)
ax.plot(x1, y1, 'g', label="Исходный сигнал")
ax.set_xlabel("Время, c")
ax.set_ylabel('Напряжение')

task(x1,y1)

end = time.time()
print((end-start) * 10**3, "ms")
plt.show()